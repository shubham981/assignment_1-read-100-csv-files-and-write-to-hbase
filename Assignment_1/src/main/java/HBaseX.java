import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;

import java.io.IOException;

public class HBaseX {
    private Configuration conf;
    private HTableDescriptor ht;
    private Connection connection;

    public Table gethTable() {
        return hTable;
    }

    private Table hTable;

    public HTableDescriptor getHt() {
        return ht;
    }

    public Admin getAdmin() {
        return admin;
    }

    private Admin admin;

    public HBaseX(String name) throws IOException {
        conf = HBaseConfiguration.create(new Configuration());
        ht = new HTableDescriptor(TableName.valueOf(name));
        connection = ConnectionFactory.createConnection(conf);
        admin = connection.getAdmin();
        hTable = connection.getTable(TableName.valueOf(name));

    }

    public void addcolumn(String name) {
        ht.addFamily(new HColumnDescriptor(name));
    }

}
