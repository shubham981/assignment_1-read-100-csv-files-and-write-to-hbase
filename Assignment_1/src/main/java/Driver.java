import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Driver extends Configured implements Tool {

    @Override
    public int run(String[] strings) throws Exception {
        Job job = setup();
        Read_Data read = new Read_Data();
        HBaseX base = new HBaseX("Employee");
        base.addcolumn("Info");
        base.getAdmin().createTable(base.getHt());
        Path[] file = read.getPathList(read.getFileStatus());
        String rowname = null;
        StringTokenizer tokens;
        for (Path path : file) {
            try {
                FileSystem fs = FileSystem.get(job.getConfiguration());
                BufferedReader br = new BufferedReader(new InputStreamReader(fs.open(path)));
                String line, Emp_Id;
                System.out.println(path + "   path    ");
                line = br.readLine();
                while (line != null) {
                    tokens = new StringTokenizer(line, ",");
                    Emp_Id = tokens.nextToken();
                    Put p = new Put(Bytes.toBytes(Emp_Id));
                    p.addColumn(Bytes.toBytes("Info"), Bytes.toBytes("Emp_Id"), Bytes.toBytes(Emp_Id));
                    p.addColumn(Bytes.toBytes("Info"), Bytes.toBytes("Emp_Name"), Bytes.toBytes(tokens.nextToken()));
                    p.addColumn(Bytes.toBytes("Info"), Bytes.toBytes("Emp_Age"), Bytes.toBytes(tokens.nextToken()));
                    p.addColumn(Bytes.toBytes("Info"), Bytes.toBytes("Emp_Email_Id"), Bytes.toBytes(tokens.nextToken()));
                    p.addColumn(Bytes.toBytes("Info"), Bytes.toBytes("Emp_Phone"), Bytes.toBytes(tokens.nextToken()));
                    p.addColumn(Bytes.toBytes("Info"), Bytes.toBytes("Emp_City"), Bytes.toBytes(tokens.nextToken()));
                    System.out.println(line);
                    base.gethTable().put(p);
                    line = br.readLine();
                }

                br.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        base.gethTable().close();
        System.exit(job.waitForCompletion(true) ? 0 : 1);
        return 0;
    }

    private Job setup() throws IOException {
        Configuration con;
        Job job;
        con = new Configuration();
        job = Job.getInstance(con, "Driver");
        job.setJarByClass(Driver.class);
        return job;
    }

    public static void main(String[] args) throws Exception {
        ToolRunner.run(new Driver(), args);

    }


}
