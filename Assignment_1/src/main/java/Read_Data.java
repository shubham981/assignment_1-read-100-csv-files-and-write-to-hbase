import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class Read_Data {
    private Configuration configuration;
    private FileSystem hdfs;
    private FileStatus[] fileStatus;

    public FileStatus[] getFileStatus() {
        return fileStatus;
    }

    public Read_Data() throws IOException, URISyntaxException {
        //1. Get the Configuration instance
        configuration = new Configuration();
        //2. Get the instance of the HDFS
        hdfs = FileSystem.get(new URI("hdfs://localhost:9000"), configuration);
        //3. Get the metadata of the desired directory
        fileStatus = hdfs.listStatus(new Path("hdfs://localhost:9000/user/data/dir2/temp/"));

    }

    public Path[] getPathList(FileStatus[] fileStatus) {     //4. Using FileUtil, getting the Paths for all
        Path[] paths = FileUtil.stat2Paths(fileStatus);
        return paths;
    }
}